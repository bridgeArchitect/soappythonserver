from flask import Flask
from flask_spyne import Spyne
from spyne.protocol.soap import Soap11
from spyne.model.primitive import Unicode, Integer
from spyne.model.complex import Iterable
import os

# create SOAP-service
app = Flask(__name__)
spyne = Spyne(app)
# path to file of database
DB_FILE_NAME = '/home/bridgearchitect/PythonPrograms/SoapClient/users.txt'

# class for SOAP-service
class SomeSoapService(spyne.Service):

	# url-path of SOAP-service
    __service_url_path__ = '/soap/someservice'
    # xml-protocol for SOAP
    __in_protocol__ = Soap11(validator='lxml')
    __out_protocol__ = Soap11()

    # function to check user
    @spyne.srpc(str, _returns=bool)
    def isLoginFree(login):

        # check file of database
        if not os.path.exists(DB_FILE_NAME):
            return True

        # receive all users from file
        with open(DB_FILE_NAME) as f:
            content = [x.strip() for x in f.readlines()]

        # find given user
        return login not in content

    # function to add user
    @spyne.srpc(str, _returns=str)
    def addLogin(login):

        # check file of database
        if os.path.exists(DB_FILE_NAME):
            # receive all users from file
            with open(DB_FILE_NAME) as f:
                content = [x.strip() for x in f.readlines()]
            # if this user exists then we will write error
            if login in content:
                return 'This login already exists'

        # add new user
        with open(DB_FILE_NAME, 'a' if os.path.exists(DB_FILE_NAME) else 'w') as f:
            f.write(login + '\n')

        # return name of new user
        return login

    # function to update user
    @spyne.srpc(str, str, _returns=bool)
    def updateLogin(oldLogin, newLogin):

        # indicator and row for users
        ind = False
        users = ""

        # check file of database
        if os.path.exists(DB_FILE_NAME):

            # open file of database
            with open(DB_FILE_NAME) as f:

                # read all users
                line = f.readline()
                while line:
                    # check if this exists
                    if line.rstrip("\n") == oldLogin:
                        ind = True
                    else:
                        # add new user in row
                        users = users + line
                    line = f.readline()
                f.close()

            # remove file of database
            os.remove(DB_FILE_NAME)

            # create file of database
            with open(DB_FILE_NAME, 'w') as f:
                # write row in file with new login if user exists
                if ind:
                    f.write(users + newLogin + '\n')
                    # return success of updating
                    return True
                # write row in file without new login if user exists
                else:
                    f.write(users)
                f.close()

        # return fail of updating
        return False

# launch SOAP-service
if __name__ == '__main__':
    app.run(host = '0.0.0.0', port=5000)